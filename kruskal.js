function kruskal(nodes, edges) {
    var result = [];
    // [[A], [B], [C]]
    var forest = _.map(nodes, function(node) { return [node.id]; });
    //sort by the weight
    var sortedEdges = _.sortBy(edges, function(edge) { return -edge[2]; });
    while(forest.length > 1) {
        var edge = sortedEdges.pop();

        var n1 = edge[0],
            n2 = edge[1];
        var t1 = _.filter(forest, function(tree) {
            return _.contains(tree, n1);
        });

        var t2 = _.filter(forest, function(tree) {
            return _.contains(tree, n2);
        });

        
        if (JSON.stringify(t1) != JSON.stringify(t2)) {
            forest = _.without(forest, t1[0], t2[0]);
            forest.push(_.union(t1[0], t2[0]));
            result.push(edge);
        }
    }
    return result;
}