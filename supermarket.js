$(document).ready(function() {

    var nodes = [
        {
            id: 'A',
            top: 314,
            left: 195
        },
        {
            id: 'B',
            top: 314,
            left: 370
        },
        {
            id: 'C',
            top: 300,
            left: 570
        },
        {
            id: 'D',
            top: 167,
            left: 66 
        },
        {
            id: 'E',
            top: 162,
            left: 284
        },
        {
            id: 'F',
            top: 22,
            left: 290 
        },
        {
            id: 'G',
            top: 120,
            left: 448
        },
        {
            id: 'H',
            top: 161,
            left: 625
        },
        {
            id: 'I',
            top: 275,
            left: 720
        },
        {
            id: 'J',
            top: 17,
            left: 590
        }]
    
    initSupermarket();

    $('.btn-kruskal').click(function() {
        $('.btn-restart').click();
        var edges = $('#nodes').val().split(';');
        edges = _.map(edges, function(edge){
            return _.map(edge.split(","), function(str) { return str.trim()});
        })
        drawLines(nodes, edges, 'black');  
        var result = kruskal(nodes, edges);
        drawLines(nodes, result, 'red');
        $("#nodes-result").val(result.join(';\n'));
    });

    $('.btn-restart').click(function(){
        $('.line').each(function(element) {
            $(this).remove();
        });
        initSupermarket()
    });

    function initSupermarket() {
        //draw all the dots
        for(var i=0;i<nodes.length;i++) {
            css = {
                left: nodes[i].left-6,
                top: nodes[i].top-17,
                zIndex: nodes.length-i 
            }
            // html/css for the vertice
            div = $('<div id="dot_container_'+i+'" order_value="'+i+'" class="dot_container"><div class="dot"></div><div class="dot_number">'+nodes[i].id+'</div></div>').css(css);
            //add the vertice to the page
            $('.supermarket').append(div);
        }
    }

    function drawLines(nodes, edges, color) {
        for(var i = 0; i < edges.length; i++) {
            var node1 = getById(nodes, edges[i][0]);
            var node2 = getById(nodes, edges[i][1]);
            var weight = edges[i][2];
            
            if (!node1 || !node2) 
                continue;
            
            draw(node1.top, node2.top, node1.left, node2.left, weight, color);
        }
    }

    function getById(nodes, id) {
        for (var i = 0; i<nodes.length; i++) {
            if (nodes[i].id != id) {
                continue;
            }
            return nodes[i];
        }
    }

    function draw(y1, y2, x1, x2, weight, color) {
        var m = (y2-y1)/(x2-x1); //slope of the segment
        var angle = (Math.atan(m))*180/(Math.PI); //angle of the line
        var d = Math.sqrt(((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1))); //length of the segment
        var transform;

        // the (css) transform angle depends on the direction of movement of the line
        if (x2 >= x1){
                transform = (360 + angle) % 360;
        } else {
                transform = 180 + angle;
        }

        // add the (currently invisible) line to the page
        var id ='line_'+new Date().getTime()+y1+y2+x1+x2;
        var line = "<div id='"+id+"'class='line' style='background-color: "+color+";'>&nbsp;<h4 style='margin-left:100px; color: red; margin-top: 1px;'>"+weight+"</h4></div>";
        $('.supermarket').append(line);
        
        //rotate the line
        $('#'+id).css({
            'left': x1,
            'top': y1,
            'width': '0px',
            'transform' : 'rotate('+transform+'deg)',
            'transform-origin' : '0px 0px',
            '-ms-transform' : 'rotate('+transform+'deg)',
            '-ms-transform-origin' : '0px 0px',
            '-moz-transform' : 'rotate('+transform+'deg)',
            '-moz-transform-origin' : '0px 0px',
            '-webkit-transform' : 'rotate('+transform+'deg)',
            '-webkit-transform-origin' : '0px 0px',
            '-o-transform' : 'rotate('+transform+'deg)',
            '-o-transform-origin' : '0px 0px'
        });

        // 'draw' the line
        $('#'+id).animate({
            width: d,
            }, 400, "linear", function(){
                    console.log("finished animation.")
                });
    }
});

